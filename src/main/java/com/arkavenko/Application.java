package com.arkavenko;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        System.out.println("Enter Size:");
        int size = scn.nextInt();
        int[] arr = new int[size];
        System.out.println();
        System.out.println("Enter values:");
        for (int i = 0; i < size; i++) {
            arr[i] = scn.nextInt();
            System.out.println();
        }
        for (int i = 0; i < size; i++) {
            if (arr[i] % 2 == 0) {
                System.out.println(arr[i] + ",");
            }
        }
    }
}
